package com.tmt.spring5webapp.bootstrap;

import com.tmt.spring5webapp.domain.Author;
import com.tmt.spring5webapp.domain.Book;
import com.tmt.spring5webapp.domain.Publisher;
import com.tmt.spring5webapp.repository.AuthorRepository;
import com.tmt.spring5webapp.repository.BookRepository;
import com.tmt.spring5webapp.repository.PublisherRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class BootStrapData implements CommandLineRunner {

    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;
    private final PublisherRepository publisherRepository;

    public BootStrapData(AuthorRepository authorRepository, BookRepository bookRepository, PublisherRepository publisherRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        this.publisherRepository = publisherRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        Publisher publisher = new Publisher();
        publisher.setName("ppp");
        publisher.setAddressline("111");
        publisher.setCity("hyd");
        publisher.setCountry("india");
        publisher.setZip("123");
        publisherRepository.save(publisher);

        System.out.println("publisher count: " +publisherRepository.count());
        Author eric = new Author("mark","jean");
        Book ddd = new Book("Domain Drive Design", "1234");

        eric.getBooks().add(ddd);
        ddd.getAuthors().add(eric);

        ddd.setPublisher(publisher);
        publisher.getBooks().add(ddd);


        authorRepository.save(eric);
        bookRepository.save(ddd);
        publisherRepository.save(publisher);

        Author rod = new Author("j2ee","123");
        Book noJE = new Book("j2ee development","123");
        rod.getBooks().add(noJE);
        noJE.getAuthors().add(rod);
        noJE.setPublisher(publisher);
        publisher.getBooks().add(noJE);

        authorRepository.save(rod);
        bookRepository.save(noJE);

        System.out.println("books Count: "+bookRepository.count());
        System.out.println("Books Published By Publisher: " +publisher.getBooks().size());

    }
}
